import './App.css';
import React, { Component } from 'react'
import TableData from './components/TableData';
import RegisterForm from './components/RegisterForm';
import { Container, Row,Col } from 'react-bootstrap';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          id: 1,
          userName: "Seth",
          email:"seth009@gmail.com",
          gender:"Male"
        },
        {
          id: 2,
          userName: "Ratha",
          email:"ratha110@gmail.com",
          gender:"Male"
        },
        {
          id: 3,
          userName: "Pisey",
          email:"pisey009@gmail.com",
          gender:"female"
        },
        {
          id: 4,
          userName: "Narak",
          email:"narakkoko12@gmail.com",
          gender:"female"
        }
      ]
    }
    this.onAdd = this.onAdd.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }

  onAdd() {
    let data = this.state.data
    this.setState({
      data: data
    })
  }
  onDelete(i) {
    let data = this.state.data
    data.slice(i,1)
    this.setState({
      data: data
    })
  }

  render() {
    return (
      <Container>
        <Row>
          <Col md={5}><RegisterForm/></Col>
          <Col md={7}><TableData data={this.state.data} onAdd={this.onAdd}/></Col>
        </Row>
      </Container>
    )
  }
}

