import React from 'react'
import { Form, Button, InputGroup } from 'react-bootstrap'
import { UserOutlined } from '@ant-design/icons'

export default function RegisterForm(props) {
    return (
        <div className="mt-5">
            <h5>KSHRD Student</h5>
            <Form>
                <div className="cover" style={{ textAlign: "center" }}>
                    <UserOutlined style={{ fontSize: "5em" }} /><br /><br />
                    <h6>Create Account</h6>
                </div>
                <br />
                <Form.Group controlId="formBasicUserName">
                    <Form.Label>User Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter User name" name="name" />
                </Form.Group>
                <InputGroup>
                    <Form.Group controlId="formBasicGender">
                        <Form.Label>Gender</Form.Label>
                        <div className="d-flex">
                            <Form.Check
                                type="radio"
                                label="Male"
                                name="group2"
                                id="radio1"
                                className="mr-4"
                            />
                            <Form.Check
                                type="radio"
                                label="Female"
                                name="group2"
                                id="radio1"
                            />
                        </div>
                    </Form.Group>
                </InputGroup>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" placeholder="Enter Email" />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Button variant="primary" type="submit" onClick={props.onAdd}>Save</Button>
            </Form>
        </div>
    )
}
