import React from 'react'
import { Container, Table, Button } from 'react-bootstrap'

function TableData(props) {
    return (
        <Container  className="mt-5">
            <h5>Table Account</h5>
            <Table>
                <thead>
                    <tr>
                        <td>#</td>
                        <td>User name</td>
                        <td>Email</td>
                        <td>Gender</td>
                    </tr>
                </thead>
                <tbody>
                    {props.data.map((value, idx) => {
                        return (
                            <tr key={idx}>
                                <td>{value.id}</td>
                                <td>{value.userName}</td>
                                <td>{value.email}</td>
                                <td>{value.gender}</td>
                            </tr>  
                        )
                    })}
                </tbody>
            </Table>
            <Button variant="danger"
                onClick={() => {
                    props.onDelete()
                }}>Delete</Button>
        </Container>
    )
}

export default TableData
